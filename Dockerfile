# Specify the base image
FROM python:3.10

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file to the working directory
COPY src/requirements.txt .

# Install dependencies
RUN pip install --upgrade pip; pip install -r requirements.txt

#COPY src/main.py /app/main.py
COPY . /app

# ENTRYPOINT ["python", "src/main.py"]
CMD ["python", "src/main.py"]