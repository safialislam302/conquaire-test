#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This is the quality check for fair metrics.
#

import os
from fpdf import FPDF
from os.path import isfile


# Creates dictionary of feedback files and badge status.
# paths_dict: path of file which is checked
# path: root path of the repository
# log_path: path where feedback is stored on the server
# return: dictionary of feedback files and badge status
def create_dict_values(paths_dict, path, log_path):
    files_dict = {}
    log_path_list = []

    for val, key in paths_dict.items():
        feedback = ""
        if key:
            files_dict[key] = 2
            feedback = "SUCCESS: The {} file exists. Please make sure that it contains the necessary information.".format(
                val)
            val = str(key).rsplit("/")[-1].rsplit(".")[0]  # get filename from path
        else:
            tmp_key = path + "/" + val
            files_dict[tmp_key] = 0
            feedback = "ERROR: The {} file, does not exist.\n" \
                       "Please create and place it in the project root " \
                       "directory.".format(val)
        output_path = log_path + "/" + val + "_.log"
        create_log(output_path, val + "_.log", feedback)
        log_path_list.append(log_path + "/" + val)

    create_pdf(log_path_list, path)

    return files_dict


# Writes the results to a file.
# logfile: path of the logfile on the server
# content: feedback which is written into file
def create_log(logfile, logfile_new, content):
    if not os.path.exists('public'):
        os.makedirs('public')

    with open(logfile, 'w+') as log_file:
        log_file.write(str(content))

    with open("public/" + logfile_new, 'w+') as log_file:
        # log_file.write(str(content))
        log_file.write(content)

title = 'Fair Check'

class PDF_fair(FPDF):
    def header(self):
        if self.page_no() == 1:
            self.set_font('times', 'B', 25)
            title_w = self.get_string_width(title)
            doc_w = self.w
            self.set_x((doc_w - title_w) / 2)
            # self.set_fill_color(135, 206, 250)  # background
            self.set_text_color(0, 0, 0)  # text
            self.cell(title_w, 10, title, ln=1, align='C')  # fill=1
            self.ln(10)

    def footer(self):
        self.set_y(-15)
        self.set_font('times', 'B', 10)
        self.cell(0, 10, f'page {self.page_no()}', align='C')

    def chapter_body(self, txt, name):
        file_name = str(name).rsplit("/")[-1].rsplit("_")[0]
        self.set_left_margin(20)
        self.set_right_margin(50)
        self.set_top_margin(25)
        self.set_font('times', 'B', 16)
        self.cell(0, 20, 'File Name: ' + file_name)
        self.ln(20)
        self.set_font('times', '', 12)
        self.multi_cell(0, 5, txt)
        self.ln(5)
        self.set_font('times', 'B', 16)
        # if isfile('Public' + file_name + ):
        # self.cell(0, 20, '[Source File]', link=name)
        self.ln(30)

    def source_file(self, name):
        self.set_font('times', 'B', 25)
        self.cell(0, 20, '[Source File]', link=name)


def create_pdf(logfiles, path):
    pdf_fair = PDF_fair('P', 'mm', 'A4')
    pdf_fair.set_auto_page_break(auto=True, margin=35)
    pdf_fair.add_page()
    # pdf.set_font('times', '', 16)
    # pdf.cell(10, 10, feedback, ln=True, border=True)
    for file in logfiles:
        with open(file + "_.log", 'r') as f:
            contents = f.read()
        file_name = str(file).rsplit("/")[-1]
        pdf_fair.chapter_body(contents, path + '/' + file_name + ".txt")

    file_path = os.path.dirname(file)
    pdf_fair.output(file_path + '/output_fair.pdf')
    pdf_fair.output('public' + '/output_fair.pdf')
