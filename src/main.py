#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Main script to execute quality check pipeline.
# It calls all the checks, creates the feedback and sends an email to the user.
#

import os
import argparse
from os.path import isfile
from subprocess import Popen, PIPE

import fair_check.fair_check as fc
import csv_check.csv_check as cc
import xml_check.xml_check as xc
import html_feedback.make_overview as mo
import mail.send_mail as sm
 
from pathlib import Path
from os import listdir

# Argument parser is used to get all needed paths and user specific CI variables.
parser = argparse.ArgumentParser(description="Process csv checking and feedback")
parser.add_argument("--feedback", "-f", dest="feedback", type=str, required=True,
                    help="Set the path on the server where the feedback files are stored.")
parser.add_argument("--link", "-l", dest="link", type=str, required=True,
                    help="Set the webpage where the feedback_folder is shown.")
parser.add_argument("--root_path", "-r", dest="root_path", type=str, required=True,
                    help="Set the path to the repository.")
parser.add_argument("--data", "-d", dest="data", type=str, required=False,
                    help="Set the path to the data inside the repository.")
parser.add_argument("--gitlab_user_name", "-gn", dest="gitlab_user_name", type=str, required=True,
                    help="Set the user name.")
parser.add_argument("--gitlab_user_email", "-ge", dest="gitlab_user_email", type=str, required=True,
                    help="Set the user email.")
parser.add_argument("--gitlab_project_url", "-gu", dest="gitlab_project_url", type=str, required=True,
                    help="Set the url to the gitlab project.")
parser.add_argument("--gitlab_project_path", "-gp", dest="gitlab_project_path", type=str, required=True,
                    help="Set gitlab path for the project.")
parser.add_argument("--gitlab_commit_sha", "-gs", dest="gitlab_commit_sha", type=str, required=True,
                    help="Set the commit id.")
args = parser.parse_args()

# Create paths from argparser as string variables.
s_path = args.feedback # "/builds/safialislam302/conquaire-test/results" # 
w_path = args.link # "http://conquaire.uni-bielefeld.de/feedback/" # 
r_path_temp = args.root_path # "/builds/safialislam302/conquaire-test" # 
#print(r_path_temp)
r_path = r_path_temp.replace('\\', '/')
d = args.data # "data/" # 
d_path = os.path.join(r_path, d) # r_path + "/" + d  # 
gitlab_user_name = args.gitlab_user_name # "Safial Islam" # 
gitlab_user_email = args.gitlab_user_email # "safialislam302@gmail.com" # 
gitlab_project_url = args.gitlab_project_url # "https://gitup.uni-potsdam.de/CRC1294/Z03/experimental/conquaire_quality_checks" # 
gitlab_project_path = args.gitlab_project_path # "user/quality_checks" # 
gitlab_commit_sha = args.gitlab_commit_sha # "1234abcde_sha" # 
server_path = s_path + "/" + gitlab_project_path + "/" + gitlab_commit_sha
# os.path.join(s_path, gitlab_project_path, gitlab_commit_sha)
website_path = os.path.join(w_path, gitlab_project_path, gitlab_commit_sha)
#print(r_path)
#print(d_path)

# Main function executes everything.
def main():
    # Create feedback path on the server where feedback is stored.
    os.makedirs(server_path, exist_ok=True)

    # Search and select files which should be checked.
    fair_files = ["AUTHORS", "LICENSE", "README"]
    fair_dict = {}
    for path in fair_files:
        print("R_PATH: ", r_path)
        # Check for plain file name "AUTHORS" without extension or "AUTHORS.txt/md" but only accept one option.
        if find_file(r_path, path + ".md"):
            fair_dict[path] = find_file(r_path, path + ".md")
            print(".md: ", fair_dict[path])
        elif find_file(r_path, path + ".txt"):
            fair_dict[path] = find_file(r_path, path + ".txt")
            print(".txt: ", fair_dict[path])
        else:
            fair_dict[path] = find_file(r_path, path)
            print("others: ", fair_dict[path])

    # Create html and log files from feedback.
    mo.init(r_path, server_path)

    # Get file paths and feedback if file was found.
    fair_result_dict = fc.create_dict_values(fair_dict, r_path, server_path)

    # Create html and log files from feedback.
    mo.add_section("FAIR metrics", gitlab_project_path, fair_result_dict)

    csv_file = 'csv_data'
    d_path_new = d_path + csv_file
    filenames = find_file_csv_xml(d_path_new, ".csv")
    if(filenames):
        csv_dict = {}
        for csv_files in filenames:
            csv_file_loc = d_path_new + "/" + csv_files
            # for path in csv_file_loc:
            p = csv_file_loc.rsplit("/", 1)
            csv_dict[csv_file_loc] = find_file(p[0], p[1].rsplit(".", 1)[0] + ".ini")

        # Get file paths and feedback if file was found.
        csv_result_dict = cc.create_dict_values(csv_dict, r_path, server_path)
        # Create html and log files from feedback.
        mo.add_section("CSV checks", gitlab_project_path, csv_result_dict, display_htm=True)



    xml_file = 'xml_data'
    d_path_new_xml = d_path + xml_file
    print("d_path_new_xml: ", d_path_new_xml)
    xml_files = find_file_csv_xml(d_path_new_xml, ".xml")
    print("xml files: ", xml_files)
    if(xml_files):
        xml_dict = {}
        for path in xml_files:
            xml_file_loc = d_path_new_xml + "/" + path
            p = xml_file_loc.rsplit("/", 1)
            xml_dict[xml_file_loc] = find_file(p[0], p[1].rsplit(".", 1)[0] + ".dtd")

        # Get file paths and feedback if file was found.
        xml_result_dict = xc.create_dict_values(xml_dict, r_path, server_path)
        # Create html and log files from feedback.
        mo.add_section("XML checks", gitlab_project_path, xml_result_dict, display_htm=True)

    badge_status = mo.finalize(website_path)

    folder_path = 'public'
    file_names = os.listdir(folder_path)

    with open('public/index.html', 'w') as f:
        f.write('<!DOCTYPE html>\n')
        f.write("<html>\n")
        f.write('<head>\n')
        f.write('<link rel="stylesheet" type="text/css" href="style.css">\n')
        f.write("<style>")
        f.write("a:visited {color: blue; text-decoration: underline;}")
        f.write("</style>")
        f.write('</head>\n')
        f.write("<body>\n")
        f.write(f'<h1 style="font-size: 30px;">File List</h1>\n')
        f.write("<ul>\n")
        for file in file_names:
            f.write(f'<li style="font-size: 24px;"> <a href="{file}">{file}</a></li>\n')
            f.write("<br>")
        f.write("</ul>\n")
        f.write("</body>\n")
        f.write("</html>\n")



    # Create mail content and send email to the user.
    # mail_content = sm.create_content(gitlab_user_name, gitlab_project_url, gitlab_commit_sha, website_path, badge_status)
    # sm.send_mail(gitlab_user_email, gitlab_project_url, mail_content)


# Use bash command to search a path for a file.
# path: directory which is searched
# file_name: file name which is searched
# return: return path for a searched file.
# change the function with easy function
'''def find_file_csv2(path, file_name):
    p = Popen(["find", path, "-type", "f", "-iname", file_name], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    p.wait()
    output, _ = p.communicate()
    file_path = str(output).strip("b'").split("\\n")
    file_path.remove("")
    return file_path'''


def find_file(path, file_name):
    file_path = path + "/" + file_name

    if isfile(file_path):
        return file_path
    else:
        return 0


def find_file_csv_xml(path, file_name):
    #print("Path: " + path + " File name: " + file_name)
    filenames = listdir(path)
    #print("File name: ", filenames)
    return [filename for filename in filenames if filename.endswith(file_name)]


if __name__ == "__main__":
    main()
