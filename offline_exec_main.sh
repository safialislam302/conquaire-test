#!/usr/bin/env bash

# CI VARIABLES: "${GITLAB_USER_NAME}", "${GITLAB_USER_EMAIL}", "${CI_COMMIT_SHA}", "${CI_PROJECT_URL}", "${CI_PROJECT_PATH}"

# feedback path where resulting files are stored
FB="$(pwd)/results/"
# url where results can be accessed via browser (not necessary for offline testing)
LI="http://conquaire.uni-bielefeld.de/feedback/"
# user name
GN="Max_Mustermann"
# mail adress of user
GE="user@mail.com"
# url of the gitlab project
GU="https://gitlab.ub.uni-bielefeld.de/user/quality_checks"
# project path
GP="user/quality_checks"
# commit sha
GS="1234abcde_sha"

# execute python scripts
python src/main.py -f "${FB}" -l "${LI}" -r "$(pwd)" -d "data/" -gn "${GN}" -ge "${GE}" -gu "${GU}" -gp "${GP}" -gs "${GS}"

#docker run -it --rm --name conquaireqc -v "$PWD/testrun:/results" --user root conquaireqc python3 main.py -f "${FB}" -l "${LI}" -r "$(pwd)" -d "data/" -gn "${GN}" -ge "${GE}" -gu "${GU}" -gp "${GP}" -gs "${GS}"